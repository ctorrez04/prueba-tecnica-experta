package hello.services.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MatrizServiceImplTest {

	MatrizServiceImpl service = new MatrizServiceImpl();
	
	/*
	 * Usar assertEquals con valores UPDATE y Query a ejecutar y valor esperado
	 * En consola se imprimaran los valores de entrada y salida
	 * Ej:
	 * 		IN = UPDATE [2, 2, 2, 1]
	 * 		OUT = SUCCESS
	 * 
	*/
	@Test
	public void testExecute() {
		assertEquals(service.execute("UPDATE",2, 2, 2, 4), "SUCCESS");
		assertEquals(service.execute("QUERY", 1, 1, 1, 3, 3, 3), "SUCCESS / 4");
		assertEquals(service.execute("UPDATE", 1, 1, 1, 23), "SUCCESS");
		assertEquals(service.execute("QUERY", 2, 2, 2, 4, 4, 4), "SUCCESS / 4");
		assertEquals(service.execute("QUERY", 1, 1, 1, 3, 3, 3), "SUCCESS / 27");
		assertEquals(service.execute("UPDATE", 2, 2, 2, 1 ),"SUCCESS");
		assertEquals(service.execute("QUERY", 1, 1, 1, 1, 1, 1), "SUCCESS / 23");
		assertEquals(service.execute("QUERY", 1, 1, 1, 2, 2, 2), "SUCCESS / 24");
		assertEquals(service.execute("QUERY", 2, 2, 2, 2, 2, 2), "SUCCESS / 1");
	}
}
