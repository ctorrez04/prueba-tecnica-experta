package hello.services.impl;

import java.util.Arrays;

import org.springframework.stereotype.Service;

import hello.services.MatrizServices;

@Service
public class MatrizServiceImpl implements MatrizServices {
	
	public String makeUpdate(int x, int y, int z, int w) {
		mat.setValue(x,y,z,w);
		return successMessage;
	}
	public String makeQuery(int x1, int y1, int z1,int x2, int y2, int z2) {
		long sum = 0; 
		for(int i=x1; i<=x2;i++) {
			for(int j=y1; j<=y2;j++) {
				for(int k=z1; k<=z2;k++) {
					sum+=mat.getValue(i,j,k);
				}
			}	
		}
		return successMessage + " / " + sum;
	}
	public String execute(String statement, int... params) {
		String result = "";
		System.out.println("IN = "+statement + " "+Arrays.toString(params));
		if(statement.equalsIgnoreCase("UPDATE") && params.length==4 && paramsInRange(params)) {
			result = makeUpdate(params[0]-1,params[1]-1,params[2]-1,params[3]);
			System.out.println("OUT = "+result);
			return result;
		}
		else if(statement.equalsIgnoreCase("QUERY") && params.length==6 && paramsInRange(params)) {
			result = makeQuery(params[0]-1,params[1]-1,params[2]-1,params[3]-1,params[4]-1,params[5]-1);
			System.out.println("OUT = "+result);
			return result;
		}else {
			System.out.println("OUT = "+errorMessage);
			return errorMessage;
		}
	}
	boolean paramsInRange(int... params) {
		int max = params.length;
		for(int i=0; i<max;i++) {
			if(max == 4 && i == max-1 && params[i]<-126 || params[i]>126) {
				return false;
			}
			if( params[i]<-100 || params[i]>100) {
				return false;
			}
		}
		return true;
	}
}
