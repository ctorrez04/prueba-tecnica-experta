package hello.services;

import hello.models.Matriz;

public interface MatrizServices {
	String successMessage = "SUCCESS";
	String errorMessage = "ERROR";
	public static Matriz mat = new Matriz();
	String makeUpdate(int x, int y, int z, int w);
	String makeQuery(int x1, int x2, int y1,int y2, int z1, int z2);
	String execute(String statement, int... params);
}
