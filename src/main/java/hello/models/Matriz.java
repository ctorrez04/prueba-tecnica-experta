package hello.models;

public class Matriz {
	private int[][][] matriz = new int[100][100][100];
	public Matriz(){
		for(int i=0;i<100;i++) {
			for(int j=0;j<100;j++) {
				for(int k=0;k<100;k++) {
					matriz[i][j][k]=0;
				}
			}
		}
	}
	public int[][][] getMatriz() {
		return matriz;
	}

	public void setMatriz(int[][][] matriz) {
		this.matriz = matriz;
	}
	public int getValue(int x, int y, int z) {
		return matriz[x][y][z];
	}
	public void setValue(int x, int y, int z, int w) {
		matriz[x][y][z] = w;
	}
}
